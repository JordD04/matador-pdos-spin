# coding: utf-8
# Distributed under the terms of the MIT License.

""" `matador` performs, aggregates, and analyses high-throughput
electronic structure calculations, primarily for crystal structure
prediction, leveraging CASTEP and Quantum Epsresso as density-functional
theory compute engines.
"""


__all__ = ['__version__']
__author__ = 'Matthew Evans'
__maintainer__ = 'Matthew Evans'


from pkg_resources import require
try:
    __version__ = require('matador')[0].version
except Exception:
    __version__ = 'xxx'
