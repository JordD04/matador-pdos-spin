.. include:: readme.rst

.. toctree::
    :hidden:
    :maxdepth: 4
    
    install
    getting_started
    changelog
    tutorials_index
    Python API <modules>
